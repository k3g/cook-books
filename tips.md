# Tips

## Admin panel

### Outbound requests

- go to http://gitlab.gitlab-dev.svc/admin/application_settings/network
- expand **Outbound requests** panel
- check `Allow requests to the local network from web hooks and services`
- check `Allow requests to the local network from system hooks`
- save changes
