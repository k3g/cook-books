# Some commands

- start the cluster: `export KUBECONFIG=$(k3d get-kubeconfig -n cluster_name); k3d start --name cluster_name`
- stop the cluster: `k3d stop --name cluster_name`